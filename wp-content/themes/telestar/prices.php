<div class="prices">
            <div class="prices-title">ВИРТУАЛЬНАЯ АТС</div>
            <div class="prices-block">
                <div class="prices-block-top"><?php echo $block1ArrayFirst['name']; ?></div>
<div class="prices-block-text">
    <div class="prices-block-text-title">Абонентская плата</div>
    <div class="prices-block-text-text">
        <div class="prices-block-text-text-price"><?php echo $block1ArrayFirst['price1']; ?> руб./мес.</div>
        Абонентская плата за номер в коде 495:
        <div class="prices-block-text-text-price"><?php echo $block1ArrayFirst['price2']; ?> руб./мес.</div>
        <div class="prices-block-text-text-line"></div>
        Количество включенных минут вызовов
        России: <div class="prices-block-text-text-minute"><?php echo $block1ArrayFirst['minute']; ?> минут</div>
    </div>
</div>
<a href="#" class="prices-block-bottom">Подключить!</a>
</div>
<div class="prices-block">
    <div class="prices-block-top"><?php echo $block1ArraySecond['name']; ?></div>
    <div class="prices-block-text">
        <div class="prices-block-text-title">Абонентская плата</div>
        <div class="prices-block-text-text">
            <div class="prices-block-text-text-price"><?php echo $block1ArraySecond['price1']; ?> руб./мес.</div>
            Абонентская плата за номер в коде 495:
            <div class="prices-block-text-text-price"><?php echo $block1ArraySecond['price2']; ?> руб./мес.</div>
            <div class="prices-block-text-text-line"></div>
            Количество включенных минут вызовов
            России: <div class="prices-block-text-text-minute"><?php echo $block1ArraySecond['minute']; ?> минут</div>
        </div>
    </div>
    <a href="#" class="prices-block-bottom">Подключить!</a>
</div>
<div class="prices-block optimal">
    <div class="prices-block-top"><?php echo $block1ArrayThird['name']; ?></div>
    <div class="prices-block-text">
        <div class="prices-block-text-title">Абонентская плата</div>
        <div class="prices-block-text-text">
            <div class="prices-block-text-text-price"><?php echo $block1ArrayThird['price1']; ?> руб./мес.</div>
            Абонентская плата за номер в коде 495:
            <div class="prices-block-text-text-price"><?php echo $block1ArrayThird['price2']; ?> руб./мес.</div>
            <div class="prices-block-text-text-line"></div>
            Количество включенных минут вызовов
            России: <div class="prices-block-text-text-minute"><?php echo $block1ArrayThird['minute']; ?> минут</div>
        </div>
    </div>
    <a href="#" class="prices-block-bottom">Подключить!</a>
</div>
<div class="prices-block">
    <div class="prices-block-top"><?php echo $block1ArrayFourth['name']; ?></div>
    <div class="prices-block-text">
        <div class="prices-block-text-title">Абонентская плата</div>
        <div class="prices-block-text-text">
            <div class="prices-block-text-text-price"><?php echo $block1ArrayFourth['price1']; ?> руб./мес.</div>
            Абонентская плата за номер в коде 495:
            <div class="prices-block-text-text-price"><?php echo $block1ArrayFourth['price2']; ?> руб./мес.</div>
            <div class="prices-block-text-text-line"></div>
            Количество включенных минут вызовов
            России: <div class="prices-block-text-text-minute"><?php echo $block1ArrayFourth['minute']; ?> минут</div>
        </div>
    </div>
    <a href="#" class="prices-block-bottom">Подключить!</a>
</div>
</div>