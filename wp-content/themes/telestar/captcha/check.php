<?php session_start();
include("random.php");
require_once('../../../../wp-config.php');
$cap = $_SESSION['captcha'];
$cap = md5($cap);
$EOL = PHP_EOL;
$boundary = "--".md5(uniqid(time()));
function check_code($code, $cookie)
{
    $code = trim($code);
    $code = md5($code);

    if ($code == $cookie){
        return TRUE;
    }
    else{
        return FALSE;
    }
}
$allowed = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);

if (isset($_POST['go']))
{

    if ($_POST['code'] == '')
    {
        echo("Ошибка: введите капчу!");
    }
    else {
        if (check_code($_POST['code'], $cap))
        {
//            $fileKeys = array_keys($_FILES['userfile']);
//            $attachArr = [];
//            $boundary = "--".md5(uniqid(time()));
//            $EOL = PHP_EOL;
//            foreach ($fileKeys as $key => $val) {
//                foreach($_FILES['userfile'][$val] as $item){
//                    $attachArr[$val][] = $item;
//                }
//            }
//            foreach($attachArr['name'] as $key => $name) {
//                $fileType = exif_imagetype($attachArr['tmp_name'][$key]);
//                $file = '';
//                if (in_array($fileType, $allowed)) {
//                    $file = chunk_split(base64_encode(file_get_contents($attachArr['tmp_name'][$key])));
//                }
//
//                $attach .=  "$EOL--$boundary$EOL";
//                $attach .= "Content-Type: application/octet-stream; name=\"$name\"$EOL";
//                $attach .= "Content-Transfer-Encoding: base64$EOL";
//                $attach .= "Content-Disposition: attachment; filename=\"$name\"$EOL";
//                $attach .= $EOL;
//                $attach .= $file;
//            }
            mailreserv(orderpreparetomail($_REQUEST), '', $boundary, $EOL);
            header('Content-Type: text/html; charset=utf-8');
            echo "Спасибо за заявку!";

        }

        else
        {
            header('Content-Type: text/html; charset=utf-8');
            echo("Ошибка: капча введена неверно!");
        }
    }

}
else
{
    echo("Access denied"); //..., возвращаем ошибку
}

function mailreserv($messageprep, $attach='', $boundary, $EOL){
    $to      = TO_ORDER_MAIL;
    $subject = iconv('UTF-8','windows-1251', ORDER_SUBJ);
    $mess  = "--$boundary$EOL";
    $mess .= "Content-Type: text/html; charset=windows-1251$EOL";
    $mess .= "Content-Transfer-Encoding: base64$EOL";
    $mess .= $EOL; // раздел между заголовками и телом html-части
    $mess .= $messageprep['mess'];
    $mess .= $attach;

    $headers  = "MIME-Version: 1.0;$EOL";
    $headers  .= "Content-Type: multipart/mixed; boundary=\"$boundary\"$EOL";
    $headers  .= 'From: ' . FROM_ORDER_MAIL . "\r\n" ;
    mail($to, $subject, $mess, $headers);
}

function orderpreparetomail ($args) {
    unset($args['go']);
    unset($args['code']);
    $email = (isset($args['req_contact_email'])) ? $args['req_contact_email'] : $args['contact_email'];
    $phone = (isset($args['req_contact_phone'])) ? $args['req_contact_phone'] : '';
    $br = '<br>';
    $mess = '<b>Клиент:</b>  ' . $args['req_contact_name'] . $br . PHP_EOL;
    $mess.= '<b>Телефон:</b>  ' . $phone . $br . PHP_EOL;
    $mess.= '<b>Email:</b>  ' . $email . $br . PHP_EOL;
    $mess.= '<b>Сайт:</b>  ' . $args['contact_site'] . $br . PHP_EOL;
    $mess.= '<b>Комментарии:</b>  ' . $args['comments'] . $br . PHP_EOL;

    return array('subj'=>iconv('UTF-8','windows-1251','Заказ'), 'mess'=>iconv('UTF-8','windows-1251', $mess));
}