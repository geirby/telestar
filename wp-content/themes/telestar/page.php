<?php get_header();
/*
Template Name: Вторая страница
*/
?>
    <div id="primary" class="content-area">
    <div class="content-main">
        <div class="content-header">
            <div class="top-slide-line"></div>
            <div class="top-breadcrumbs">
                <div class="top-breadcrumbs-container">
                    <div class="content-title"><?php echo get_the_title ()?></div>
                    <div class="breadcrumbs"><span class="breadcrumbs-span-title"></span>
                    <?php if(function_exists('bcn_display'))
                    {
                        mb_strtoupper(bcn_display());
                    }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-container">
            <? if (get_post_meta($post->ID, 'header_img', true) ) {?>
                <img class="content-container-header-img" src="<? echo get_post_meta($post->ID, 'header_img', true); ?>" />
            <? } ?>

            <div class="content-container-post">
                <?php
                    while ( have_posts() ) : the_post();
                        get_template_part( 'content', 'page' );
                            the_content();
                    endwhile;
                ?>
            </div>
            <? if (get_post_meta($post->ID, 'tariff', true) == 1) {
                require_once ('tariff-table.php');
            } ?>

            <? if (get_post_meta($post->ID, 'footer_img', true) ) {?>
                <img class="content-container-footer-img" src="<? echo get_post_meta($post->ID, 'footer_img', true); ?>" />
            <? } ?>
        </div>
    </div>
        <?php //require_once('contact-block.php') ?>
    </div>
<?php get_footer(); ?>