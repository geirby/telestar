<?php get_header();
/*
Template Name: Главная страница
*/
$block1ArrayFirst = get_option('exectariff_block1');
$block1ArraySecond = get_option('exectariff_block2');
$block1ArrayThird = get_option('exectariff_block3');
$block1ArrayFourth = get_option('exectariff_block4');
?>

<div id="primary" class="content-area">
    <div class="content-main">
        <div class="content-header">
            <div class="top-slide-line"></div>
            <div class="top-breadcrumbs">
                <div class="top-breadcrumbs-container">
                    <div class="content-title"><?php //echo get_the_title ()?></div>
                    <div class="breadcrumbs"><span class="breadcrumbs-span-title"></span>
                        <?php if(function_exists('bcn_display'))
                        {
                           // mb_strtoupper(bcn_display());
                        }?>
                    </div>
                </div>
            </div>
        </div>
        <?php echo do_shortcode('[metaslider id=29]'); ?>
        <div class="content-main-about">
<!--                <div class="content-main-about-container-title">Что мы предлагаем?</div>-->
                <div class="content-main-about-container-text">
<!--                    Сделать работу комфортнее, а бизнес эффективнее позволяет телефонизация рабочих мест офиса. мы выполняем весь комплекс работ от проекта до настройки оборудования. Вам больше не нужно думать о доступности номерной емкости ближайшей АТС или стоимости приобретения специального оборудования. Наши решения доступны каждому и реализуются на самых современных платформах: VoIP-шлюзах, виртуальных АТС и с помощью SIP-телефонов.-->
                    <?php
                    while ( have_posts() ) : the_post();
                        get_template_part( 'content', 'page' );
                        the_content();
                    endwhile;
                    ?>
                </div>
        </div>
        <? if (get_post_meta($post->ID, 'prices', true) == 1) {
            require_once('prices.php');
        } ?>
<!--        <div class="include-prices">-->
<!--            <div class="include-prices-title">Что включено в абонентскую плату?</div>-->
<!--            <div class="include-prices-text">-->
<!--                -->
<!--                -->
<!---->
<!--            </div>-->
<!--        </div>-->
    </div>

</div><!-- .content-area -->

<?php get_footer(); ?>
