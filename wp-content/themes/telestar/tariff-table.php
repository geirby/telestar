<?php
$tariffArray = [
    get_option('exectariff_block5'),
    get_option('exectariff_block6'),
    get_option('exectariff_block7'),
    get_option('exectariff_block8')
];

?>

<div class="content-container-tariff">
    <div class="content-container-tariff-list">
        <ul>
            <li class="content-container-tariff-name">Тарифный план</li>
            <li>Рабочих мест (SIP аккаунтов)</li>
            <li>Абонентская плата за пакет</li>
            <li>Тарификация исходящих звонков</li>
            <li>Функционал и сервисы ВАТС</li>
        </ul>
    </div>
    <div class="content-container-tariff-table">
        <?php foreach ($tariffArray as $value) {
            $optimal = ($value['name'] == 'Оптимальный') ? 'optimal' : '';
            echo '<div class="content-container-tariff-table-prices-block">';
                echo '<div class="content-container-tariff-table-prices-block-name ' . $optimal . '"> '. $value['name'] .  '</div>' .
                '<div>' . $value['sip'] . '</div>' .
                '<div>' . $value['price'] . '</div>' .
                '<div>' . $value['incoming'] . '</div>' .
                '<div>' . $value['service'] . '</div>' .
                '</div>';
            };
        ?>
    </div>
</div>