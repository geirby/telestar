var Popup = {
    init: function () {
        this.closed()
    },
    open: function (msg, classMsg, parent) {
        parent = parent || '';
        classMsg = classMsg || ''
        if (parent) {
            parent.append('<div class="popup"><div class="' + classMsg + '">' + msg + '</div></div>');
            $('.popup').append('<div class="close-order">x</div>');
            parent.append('<div class="overlay"></div>');
        } else {
            $('body').append('<div class="popup"><div class="' + classMsg + '">' + msg + '</div></div>');
            $('.popup').append('<div class="close-order">x</div>');
            $('body').append('<div class="overlay"></div>');
        }
        $('.popup').css({
            position: 'absolute',
            left: ($(window).width() - $('.popup').outerWidth()) / 2,
            top: ($(window).height() - $('.popup').outerHeight()) / 2 + $(document).scrollTop()
        });
    },
    closed: function (e) {
        e = e || null;
        var elemClass = '.' + $(e).attr('class');
        $('body').on('click', '.overlay, .close-order, ' + elemClass, function () {
            $('.popup, .overlay').remove();
        });
        $('body').on('keydown', '.popup', (function(eventObject){
            if (eventObject.which == 27) {
                $(this).remove();
                $('.overlay').remove();
            }
        }));
    }
};

function validate(formData, jqForm, options) {
    rcheck = true;
    $('[id^=req]').each(
        (function()
        {
            if(!this.value){
                $('#'+this.id).css('background', '#ff9a9a')
                rcheck = false

            }
            else  $('#'+this.id).css('background', '#FFF9F1')
        })
    );
    if (rcheck==false) return false;
}
function showResponse (responseText, statusText, xhr, $form){

    if (responseText == 'Ошибка: капча введена неверно!') {
        $('#response').html('')
        $('#response').append(responseText)
        $('#response').css('background', '#ff9a9a')
    }
    else {
        $('#response').html('')
        $('#response').css('background', '#AFFF98')
        $('#response').append(responseText)
        $('[id^=req]').each(
            function()
            {
                this.value = '';
            }
        );
        $("#code").val('');
    }
}



