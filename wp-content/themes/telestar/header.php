<!DOCTYPE html>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
<!--    <meta name="viewport" content="width=device-width">-->
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link rel="stylesheet" href="/wp-content/themes/telestar/css/main.css">
    <link rel="stylesheet" href="/wp-content/themes/telestar/css/responsive.css">
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php $templateurl = get_bloginfo('template_url'); ?>
    <?php wp_enqueue_script("jquery"); ?>

<!--    <link rel="stylesheet" href="/wp-content/themes/telestar/js/owl-carousel/owl.carousel.css">-->

    <!-- Default Theme -->
<!--    <link rel="stylesheet" href="/wp-content/themes/telestar/js/owl-carousel/owl.theme.css">-->


    <?php wp_head(); ?>
    <!-- Include js plugin -->
    <script type="text/javascript" src="<?php echo $templateurl ?>/js/jquery.dcjqaccordion.2.9.js"></script>
    <script src="/wp-content/themes/telestar/js/owl-carousel/owl.carousel.js"></script>
    <script src="/wp-content/themes/telestar/js/jquery.form.js"></script>
    <script src="/wp-content/themes/telestar/js/main.js"></script>
    <script>var $ = jQuery.noConflict();</script>

    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any()){
            $(document).ready(function($){
                $('#center-menu').dcAccordion({
                    eventType: 'click',
                    autoClose: false,
                    saveState: false,
                    disableLink: true,
                    showCount: false,
                    menuClose: false,
                    menuOpen: true,
                    navigation: true,
                    speed: 'fast'
                });
            });
        }

    </script>
</head>
<body <?php body_class(); ?>>
<div class="phone-block">
    <span class="phone-block-logo"><img src="/wp-content/themes/telestar/img/phone-logo.png"><span><?php echo get_option('execontact_tel1') ?></span>
        <img src="/wp-content/themes/telestar/img/phone-logo.png"><span>+7(931)313-43-50</span></span>
</div>
    <div class="wrap">
    <header>
        <a href="/" class="logo">
            <img src="/wp-content/themes/telestar/img/logo.png">
        </a>
        <div class="nav-top animenu">
            <input type="checkbox" id="button">
            <label for="button" onclick>Меню</label>
            <?php
            wp_nav_menu(array('menu' => 'main', 'depth' => 2, 'container' => '', 'menu_class' => 'center-menu', 'menu_id' => 'center-menu', 'walker' => new mainMenuWalker($upper=false)));
            ?>
        </div>
        <script type="text/javascript">

            var timeout    = 500;
            var closetimer = 0;
            var ddmenuitem = 0;

            function jsddm_open() {
                jsddm_canceltimer();
                jsddm_close();
                ddmenuitem = $(this).find('ul').css('visibility', 'visible');
                if($(this).hasClass('menu-item-has-children')) {
                    $(this).children('a').addClass('a-children')
                    $(this).children('span').addClass('a-children')
                }
            }

            function jsddm_close() {
                if (ddmenuitem) {
                    ddmenuitem.css('visibility', 'hidden');
                    $(ddmenuitem).parent().find('a').removeClass('a-children')
                    $(ddmenuitem).parent().find('span').removeClass('a-children')
                    $(ddmenuitem).parent().find('span').removeClass('a-children')
//                    $('.menu-item-has-children').find('.sub-menu-line').remove()
                }

            }

            function jsddm_timer() {
                closetimer = window.setTimeout(jsddm_close, timeout);

            }

            function jsddm_canceltimer() {
                if (closetimer) {
                    window.clearTimeout(closetimer);
                    closetimer = null;
                }
            }

            $(document).ready(function() {
                $('.center-menu > li').bind('mouseover', jsddm_open);
                $('.center-menu > li').bind('mouseout',  jsddm_timer);
                $('.menu-item-has-children > a').click(function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                })

                Popup.init();
                $('.prices-block-bottom').click(function (e) {

                    e.preventDefault();
                    e.stopPropagation();
                    var elem = $(this);
                    var typeTariff = elem.parent().find('.prices-block-top').text();
                    var msg = '<form id="form-reserve" action="/wp-content/themes/telestar/captcha/check.php" method="post" enctype="multipart/form-data">' +
                        '<input type="hidden" name="comments" value=" ' + typeTariff + ' ">' +
                        '<table id="order_contact">'+
                        '<tr>'+
                        '<td><input  type="text" name="req_contact_name" id="req_contact_name"></td>'+
                        '<td><label for="req_contact_name">Ваше имя<span>*</span></label></td>'+
                        '</tr>'+
                        '<tr>'+
                        '<td><input  type="text" name="req_contact_phone" id="req_contact_phone"></td>'+
                        '<td><label for="req_contact_phone">Телефон<span>*</span></label></td>'+
                        '</tr>'+
                        '<tr>'+
                        '<td><input  type="text" name="contact_email" id="contact_email"></td>'+
                        '<td><label for="contact_email">E-mail</label></td>'+
                        '</tr>'+
                        '</table>'+
                        '<div class="container-captcha">'+
                        '<span>Защитный код:</span>'+
                        '<img src="/wp-content/themes/telestar/captcha/captcha.php" id="capcha-image">'+
                        '<a style="text-decoration: none" href="javascript:void(0);" onclick="document.getElementById(\'capcha-image\').src=\'/wp-content/themes/telestar/captcha/captcha.php?rid=\' + Math.random();">'+
                            '<img src="/wp-content/themes/telestar/captcha/refresh.png" alt="обновить каптчу" />'+
                            '</a>'+
                            '<input type="text" id="req_code" class="container-captcha-input" name="code" placeholder="введите код">'+
                            '</div>'+
                            '<input type="submit" name="go" value="Отправить">'+

                        '</form><div id="response"></div>'
                    Popup.open(msg,'popup-order');
                    $('#form-reserve').ajaxForm(
                        {
                            beforeSubmit: validate,
                            success: showResponse
                        });
                })
            });
            document.onclick = jsddm_close;



        </script>
    </header>


