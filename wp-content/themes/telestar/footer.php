<footer>
    <div class="bottom-slide-line"></div>
    <div class="footer-container">
        <a class="footer-logo" href="/">
            <img src="/wp-content/themes/telestar/img/logo-footer.png">
        </a>
        <div class="footer-left-column">
            <div class="footer-text">
                Телекоммуникационные услуги для операторов связи и корпоративных клиентов: аренда каналов связи, доступ в интернет,
                VPN, местная телефонная связь, техническая поддержка и др.
            </div>
            <div class="footer-oferta-link">
                <a href="http://telestar.spb.ru/%d0%bf%d1%83%d0%b1%d0%bb%d0%b8%d1%87%d0%bd%d0%b0%d1%8f-%d0%be%d1%84%d0%b5%d1%80%d1%82%d0%b0/">Публичная оферта</a>
            </div>
        </div>
        <div class="footer-contact">
            <div class="footer-contact-phone">
                <span>Телефон:</span>
                <span><?php echo get_option('execontact_tel1') ?> &nbsp;  +7(931)313-43-50</span>

            </div>
            <div class="footer-contact-email">
                <span>Почта:</span>
                <span><?php echo get_option('execontact_email') ?></span>
            </div>
        </div>
        <div class="footer-menu">
            <div class="footer-menu-main">
                <?php
                    $menuObject = wp_get_nav_menu_object( 'footer' );
                    if($menuObject)
                    {
                        wp_nav_menu(array('menu' => 'footer', 'depth' => 2, 'container' => 'nav', 'container_class' => 'nav nav-footer', 'menu_class' => 'footer-menu',
                            'menu_id' => 'footer-menu', 'walker' => new mainMenuWalker($upper=false)));
                    }
                ?>
            </div>
            <div class="footer-menu-spec">
                <?php
                    $menuObject = wp_get_nav_menu_object( 'footer_spec' );
                    if($menuObject)
                    {
                        wp_nav_menu(array('menu' => 'footer_spec', 'depth' => 2, 'container' => 'nav', 'container_class' => 'nav nav-footer', 'menu_class' => 'footer-menu',
                            'menu_id' => 'footer-menu', 'walker' => new mainMenuWalker($upper=false)));
                    }
                ?>
            </div>
        </div>
    </div>

    <div class="footer-boottom">
        <div class="footer-copyright">
            (с) 2010-<?php echo date("Y"); ?>  ООО “Телестар”
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<!-- wrap -->
</div>
</body>
</html>