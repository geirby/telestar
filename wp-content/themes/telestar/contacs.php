<?php get_header();
/*
Template Name: Контакты
*/
?>

    <div id="primary" class="content-area">
    <div class="content-main">
        <div class="content-header">
            <div class="top-slide-line"></div>
            <div class="top-breadcrumbs">
                <div class="top-breadcrumbs-container">
                    <div class="content-title"><?php echo get_the_title ()?></div>
                    <div class="breadcrumbs"><span class="breadcrumbs-span-title"></span>
                        <?php if(function_exists('bcn_display'))
                        {
                            mb_strtoupper(bcn_display());
                        }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-block-content">
            <div class="contact-block-map">
                <?php echo stripcslashes( get_option('execontact_map')) ?>
            </div>
        </div>
        <div class="contact-block-text">
            <?php
            while ( have_posts() ) : the_post();
                get_template_part( 'content', 'page' );
                the_content();
            endwhile;
            ?>
        </div>
        <div class="contact-block-container">

            <div class="contact-block-form">
                <div class="contact-block-name">Отправить вопрос</div>
                <form id="form-reserve" action="/wp-content/themes/telestar/captcha/check.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="subj" value="Обратная связь">
                    <table id="order_contact">
                        <tr>
                            <td><input  type="text" name="req_contact_name" id="req_contact_name"></td>
                            <td><label for="req_contact_name">Ваше имя<span>*</span></label></td>
                        </tr>
                        <tr>
                            <td><input  type="text" name="req_contact_email" id="req_contact_email"></td>
                            <td><label for="req_contact_email">E-mail<span>*</span></label</td>
                        </tr>
                        <tr>
                            <td><input  type="text" name="contact_site" id="contact_site"></td>
                            <td><label for="contact_site">Ваш сайт<span></span></label</td>
                        </tr>
                    </table>
                    <textarea rows="10" name="comments" id="req_contact_comments"></textarea><br />
                    <div class="container-captcha">
                        <span>Защитный код:</span>
                        <img src='/wp-content/themes/telestar/captcha/captcha.php' id='capcha-image'>
                        <a style="text-decoration: none" href="javascript:void(0);" onclick="document.getElementById('capcha-image').src='/wp-content/themes/telestar/captcha/captcha.php?rid=' + Math.random();">
                            <img src="/wp-content/themes/telestar/captcha/refresh.png" alt="обновить каптчу" />
                        </a>
                        <input type="text" id="req_code" class="container-captcha-input" name="code" placeholder="введите код">

                    </div>
                    <input type="submit" name="go" value="Отправить">
                </form>
                <div id="response"></div>

            </div>

            <div class="contact-block-address">
                <div class="contact-block-name">Адрес и реквизиты</div>
                <div class="copy_requisites" alt="кликните для копирования реквизитов">
                    <b class="tooltiptext">кликните для копирования реквизитов</b>
                    <span class="requisites">
                        <b>ООО "ТелеСтар"</b><br>
                        <b>ИНН</b>:  7811476065<br>
                        <b>КПП</b>:  781101001<br>
                        <b>ОГРН</b>:  1107847358486<br>
                        <b>ОКПО</b>:  67484361<br>
                        <b>Расчетный счет</b>:  40702810101000506181<br>
                        <b>Банк</b>:  Ф-Л СЕВЕРО-ЗАПАДНЫЙ ПАО БАНКА "ФК ОТКРЫТИЕ"<br>
                        <b>БИК</b>:  044030795<br>
                        <b>Корр. счет</b>:  30101810540300000795<br>
                        <b>Юридический адрес</b>:  192019, Санкт-Петербург г, Профессора Качалова, дом № 11, литера А, офис 401А<br>
                        <b>Генеральный директор</b>:  Володина Юлия Александровна<br>
                    </span>
                </div>
                <div class="location">
                    <span><?php echo stripcslashes( get_option('execontact_address')) ?></span>
                </div>
                <div class="phone">
                    <span><?php echo stripcslashes( get_option('execontact_tel1')) ?></span>
                </div>
                <div class="email">
                    <span><a href="mailto:<?php echo stripcslashes( get_option('execontact_email')) ?>"><?php echo stripcslashes( get_option('execontact_email')) ?></a></span>
                </div>
            </div>
            <script>
                let copyMail = document.querySelector(".copy_requisites");
                let copyIcon = document.querySelector(".copy_requisites");
                //const icon = copyIcon.querySelector(".copy_requisites");
                copyMail.addEventListener('click', function(event) {
                    console.log("clicked");
                    let copyText = document.querySelector(".requisites").innerHTML;
                    let copyTooltip = document.querySelector(".copy_requisites .tooltiptext");
                    copyTooltip.innerHTML = 'Скопировано';
                    copyIcon.classList.add("toggle-weight");
                    let textArea = document.createElement("textarea");
                    let strippedString = copyText.replace(/(<([^>]+)>)/gi, "");
                    textArea.value = strippedString.trim();
                    textArea.style.position="fixed";  //avoid scrolling to bottom
                    document.body.appendChild(textArea);
                    textArea.focus();
                    textArea.select();
                    textArea.setSelectionRange(0, 99999);
                    document.execCommand("copy");
                    document.body.removeChild(textArea);
                    setTimeout(() => {
                        copyIcon.classList.remove("toggle-weight");
                    }, 500);
                });

                copyIcon.addEventListener('mouseout', function(event) {
                    let copyTooltip = document.querySelector(".copy_requisites .tooltiptext");
                    copyTooltip.innerHTML= 'кликните для копирования реквизитов';
                });
            </script>
        </div>
    </div>
        <?php //require_once('contact-block.php') ?>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form-reserve').ajaxForm(
                {
                    beforeSubmit: validate,
                    success:       showResponse
                });
//            $('#form-reserve [id^=req]').click(function(){
//                $('#'+this.id).css('background', '#FFF9F1')
//
//            })
        });
        function validate(formData, jqForm, options) {
            rcheck = true;
            $('[id^=req]').each(
                (function()
                {
                    if(!this.value){
                        $('#'+this.id).css('background', '#ff9a9a')
                        rcheck = false

                    }
                    else  $('#'+this.id).css('background', '#FFF9F1')
                })
            );
            if (rcheck==false) return false;
        }
        function showResponse (responseText, statusText, xhr, $form){

            if (responseText == 'Ошибка: капча введена неверно!') {
                $('#response').html('')
                $('#response').append(responseText)
                $('#response').css('background', '#ff9a9a')
            }
            else {
                $('#response').html('')
                $('#response').css('background', '#AFFF98')
                $('#response').append(responseText)
                $('[id^=req]').each(
                    function()
                    {
                        this.value = '';
                    }
                );
                $("#code").val('');
            }
        }
    </script>

<?php get_footer(); ?>