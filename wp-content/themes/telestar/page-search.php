<?php get_header();
/*
Template Name: Поиск по тарифам
*/
$block1ArrayFirst = get_option('exectariff_block1');

?>

    <div id="primary" class="content-area">
        <div class="content-main">
            <div class="content-header">
                <div class="top-slide-line"></div>
                <div class="top-breadcrumbs">
                    <div class="top-breadcrumbs-container">
                        <div class="content-title"><?php echo get_the_title ()?></div>
                        <div class="breadcrumbs"><span class="breadcrumbs-span-title"></span>
                            <?php if(function_exists('bcn_display'))
                            {
                                mb_strtoupper(bcn_display());
                            }?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-container">
                <? if (get_post_meta($post->ID, 'header_img', true) ) {?>
                    <img class="content-container-header-img" src="<? echo get_post_meta($post->ID, 'header_img', true); ?>" />
                <? } ?>

                <div class="content-container-post">
                    <?php
                    while ( have_posts() ) : the_post();
                        get_template_part( 'content', 'page' );
                        the_content();
                    endwhile;
                    ?>
                </div>
                <? if (get_post_meta($post->ID, 'footer_img', true) ) {?>
                    <img class="content-container-footer-img" src="<? echo get_post_meta($post->ID, 'footer_img', true); ?>" />
                <? } ?>
            </div>
        </div>
        </div>
        <div class="search-plugin-container">
            <form class="search-form">
                <div class="search-input-block">
                    <input type="search" class="search-query" id="search" autocomplete="off">
                    <button class="search-button">Поиск</button>
                </div>
                <div class="search-tip"></div>
            </form>
            <div class="search-pagination"></div>
            <div class="search-result"></div>
        </div>
    </div><!-- .content-area -->

<?php get_footer(); ?>
<script>
    let getAjax = (action, url = '', params) => {
        let data = false;

        return $.ajax({
            url: "<?php echo admin_url("admin-ajax.php"); ?>",
            type: 'POST',
            data: `action=${action}&${params}`
            // ,
            // beforeSend: function( xhr ) {
            // },
            // success: function(data) {
            // }
    });
    }
    jQuery(function($){
        //add pagination by arrow
        $(document).keydown(function (e)
        {
            console.log(e.which);

        })
        let xTriggered = 0;
        $('#search').on('input', function () {
            xTriggered++;
            console.log(xTriggered);
            var search = $('#search').val();

            if(search.length >= 2)
            {
                let ajaxparam = 'searchstr='+$('#search').val()+'&order=region';
                getAjax('searchtip', '', ajaxparam)
                    .error(function () {
                        $('.search-tip').html('Ошибка запроса');
                    })
                    .success(function (data) {
                        let htmlEl = '<ul>';
                        for(let item in data.results) {
                            htmlEl += `<li id="${item}" class="search-tip-item">${data.results[item].region}</li>`
                        }
                        htmlEl += '</ul>';
                        $('.search-tip').html(htmlEl);
                    });
            }
        })
        $('#search').keydown(function (e)
        {
            console.log(e.which)
            if(e.which == 40)
            {
                if($(".search-tip li.active").length!=0) {
                    let storeTarget	= $('.search-tip').find("li.active").next();
                    $(".search-tip li.active").removeClass("active");
                    storeTarget.focus().addClass("active");
                    console.log(storeTarget.text());
                    $('#search').val(storeTarget.text());
                }
                else {
                    $('.search-tip').find("li:first").focus().addClass("active");
                    $('#search').val($('.search-tip').find("li:first").text());
                }
                // $('.search-tip').find('li:first').focus().addClass("active");

            }
            if (e.which == 13)
            {
                $('#search').focus();
                $('.search-tip').html('');
               // $('.search-button').trigger('click');
            }
        })

        $('.search-form').on('click', '.search-tip-item', function (e)
        {
            //console.log($(e.currentTarget).text())
            let query = $(e.currentTarget).text();
        })
        $('.search-button').click(function(e)
        {
            e.preventDefault();
            e.stopPropagation();
            $('.search-tip').html('');
            var searchStr = $('.search-form input').val();
            $.ajax({
                url: "<?php echo admin_url("admin-ajax.php"); ?>",
                type: 'POST',
                data: 'action=search&searchstr='+searchStr+'&page=1', // можно также передать в виде объекта
                beforeSend: function( xhr ) {
                $('#search-button').text('Загрузка, 5 сек...');
            },
            success: function( data ) {
                $('.search-result').html(createHtml('table', data.results, '', [{code:'Код', sortby:'desc'},{region:'Регион/Город', sortby:'desc'},{price:'Стоимость'}]));
                $('.search-pagination').html(createHtml('ul', data.pagination), '', []);
                $('.search-tip').html('');
            }
            });
        });
    });
    $('.search-pagination').on('click', '.paging-nav', function(e)
    {
        e.preventDefault();
        e.stopPropagation();
        $('.search-tip').html('');
        let searchStr = $('.search-form input').val();
        let sortBy = $(e.currentTarget).attr('href').match(/sort=([a-zA-Z]+)/)[1].toLowerCase();
        let orderBy = $(e.currentTarget).attr('href').match(/order=([a-zA-Z]+)/)[1].toLowerCase();

        console.log(orderBy);
        $.ajax({
            url: "<?php echo admin_url("admin-ajax.php"); ?>",
            type: 'POST',
            data: 'action=search&searchstr='+searchStr+'&page=' + $(e.currentTarget).attr('href').match(/page=([0-9]+)/)[1] + '&order=' + orderBy + '&sort=' + sortBy,
            beforeSend: function( xhr ) {
                $('#search-button').text('Загрузка, 5 сек...');
            },
            success: function( data ) {
                $('.search-result').html(createHtml('table', data.results, '', [{code:'Код', sortby:'asc'},{region:'Регион/Город', sortby:'asc'},{price:'Стоимость'}]));
                $('.search-pagination').html(createHtml('ul', data.pagination), '', []);
            }
        });
    });

    $(document)
        .on('click', '.search-sorting', function (e)
        {
            e.preventDefault();
            e.stopPropagation();
            let elem = $(e.currentTarget);
            let orderBy = elem.attr('href').match(/[a-z]+/)[0];
            let sortBy =  elem.attr('href').match(/[a-z]+$/)[0];
            // console.log($(e.currentTarget).attr('href').match(/[a-z]+$/)[0] + ' _   _ ');
            let ajaxparam = 'searchstr='+$('#search').val()+'&page=1&order='+orderBy+'&sort='+sortBy;
            getAjax('search', '', ajaxparam)
                .error(function () {
                    $('.search-tip').html('Ошибка запроса');
                })
                .success(function (data) {
                    sortBy = (sortBy == 'asc') ? 'desc' : 'asc';
                    $('.search-result').html(createHtml('table', data.results, '', [{code:'Код', sortby:sortBy},{region:'Регион/Город', sortby:sortBy},{price:'Стоимость'}]));
                    $('.search-pagination').html(createHtml('ul', data.pagination), '', []);
                });
    })
        .on('click', '.search-tip-item', function (e)
        {
            e.preventDefault();
            e.stopPropagation();
            let search = $(e.currentTarget).text();
            $('.search-query').val(search);
            $('.search-button').trigger('click');
            // alert(search);
        })
        .on('click', '.search-direction', function (e)
        {
            e.preventDefault();
            e.stopPropagation();
            let search = $(e.currentTarget).text();
            $('.search-query').val(search);
            $('.search-button').trigger('click');
        })
    let createHtml = (type, data, countRows, headers) =>
    {
        // console.log(data)
        let html = `<${type}>`;
        switch (type) {
            case 'table':
                let tbHead = '<tr>';
                    console.log(headers)
                    tbHead += `<th><a href="#code/${headers[0].sortby}" class="search-sorting">${headers[0].code}</a></th>

                        <th><a href="#region/${headers[1].sortby}" class="search-sorting">${headers[1].region}</a></th>
                        <th>${headers[2].price}</th>`;
                tbHead += '</tr>';
                html += tbHead;
                for(let item in data) {
                    html +=
                        `<tr id="${item}" class="search-result-item">
                            <td>${data[item].code}</td>
                            <td>${data[item].region}</td>
                            <td>${data[item].price}</td>
                        </tr>`
                }
                break;
            case 'ul':
                html += `${data}`
                break;
            case 'div' :
                break;
        }
        html += `</${type}>`;
        return html;
    }
    //createHtml('div');
</script>
