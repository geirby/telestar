<?php
/**
 *  Plugin Name: exec-tariff
 */
function execTariff_add_admin_pages ()
{
    add_options_page('Тарифы', 'Тарифы', 8, 'exectariff', 'execTariff_options_page');
}


function execTariff_options_page ()
{

//    add_option('execontact_address', 'no data');
    update_tariff();
//    print_r($_POST);
    include ('options_page.phtml');
}

function update_tariff()
{
    if(isset($_POST['setup_tariff_form_btn']))
    {
        if (function_exists('current_user_can') && !current_user_can('manage_options')) die (_e('Нет прав'));
        if ( function_exists('check_admin_referer') )
        {
            check_admin_referer('setup_tariff_form');

        }
        update_option('exectariff_block1', $_POST['exectariff_block1']);
        update_option('exectariff_block2', $_POST['exectariff_block2']);
        update_option('exectariff_block3', $_POST['exectariff_block3']);
        update_option('exectariff_block4', $_POST['exectariff_block4']);


    }
    if(isset($_POST['setup_tariff_formAtc_btn']))
    {
        if (function_exists('current_user_can') && !current_user_can('manage_options')) die (_e('Нет прав'));
        if ( function_exists('check_admin_referer') )
        {
            check_admin_referer('setup_tariff_form');
        }
        update_option('exectariff_block5', $_POST['exectariff_block5']);
        update_option('exectariff_block6', $_POST['exectariff_block6']);
        update_option('exectariff_block7', $_POST['exectariff_block7']);
        update_option('exectariff_block8', $_POST['exectariff_block8']);
    }
}


function execTariffRun ()
{
    return false;
}

add_action('admin_menu', 'execTariff_add_admin_pages');
add_action('init', 'execTariffRun');


function getTariffTagValue ($tag)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "options";
    $tag = str_replace(array('[',']'),'',$tag);
    $sql =  'select option_value from '.$table_name.' where option_name = "exectariff_'.$tag.'"';
    $rows_affected = $wpdb->get_row($sql);
    echo '<div>'.stripcslashes($rows_affected->option_value).'</div>';
}