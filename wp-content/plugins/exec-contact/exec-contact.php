<?php
/**
 *  Plugin Name: exec-contact
 */
function exeContact_add_admin_pages ()
{
    add_options_page('Контакты для страницы', 'Контакты', 8, 'execontact', 'exeContact_options_page');
}

function exeContact_options_page ()
{

    add_option('execontact_address', 'no data');
    update_contact ();
//    print_r($_POST);
    include ('options_page.phtml');
}

function update_contact () {
    if(isset($_POST['setup_contact_form_btn']))
    {
        if (function_exists('current_user_can') && !current_user_can('manage_options')) die (_e('Нет прав'));
        if ( function_exists('check_admin_referer') )
        {
            check_admin_referer('setup_contact_form');

        }
        update_option('execontact_address', $_POST['execontact_address']);
        update_option('execontact_tel1', $_POST['execontact_tel1']);
        update_option('execontact_tel2', $_POST['execontact_tel2']);
        update_option('execontact_tel3', $_POST['execontact_tel3']);
        update_option('execontact_email', $_POST['execontact_email']);
        update_option('execontact_metro', $_POST['execontact_metro']);
        $map = htmlspecialchars ($_POST['execontact_map']);
        update_option('execontact_map', $_POST['execontact_map']);

    }



}

function exeContactRun ()
{
    return false;
}

add_action('admin_menu', 'exeContact_add_admin_pages');
add_action('init', 'exeContactRun');
function getTagValue ($tag)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "options";
    $tag = str_replace(array('[',']'),'',$tag);
    $sql =  'select option_value from '.$table_name.' where option_name = "execontact_'.$tag.'"';
    $rows_affected = $wpdb->get_row($sql);
    echo '<div>'.stripcslashes($rows_affected->option_value).'</div>';
}
function _tag_($cont, $tag1, $tag2)
{
    $cont = stripslashes($cont);
    switch ($tag1) {
        case '[address]': getTagValue($tag1);
            break;
        case '[tel1]': getTagValue($tag1);
            break;
        case '[tel2]': getTagValue($tag1);
            break;
        case '[tel3]': getTagValue($tag1);
            break;
        case '[email]': getTagValue($tag1);
            break;
        case '[map]': getTagValue($tag1);
        break;
//        default: echo "no tag";
//        break;
    }

}
function parseSection($content)
{

    $tags_array = array(
        'address',
        'tel1',
        'tel2',
        'tel3',
        'email',
        'mappp'

    );
//    echo $content;
    foreach($tags_array as $tag) {
        $content2 = preg_replace("!(\[".$tag."\])!se", "_tag_('\\2', '\\1', '\\3')", $content);

    }
    echo str_replace($tags_array,'',str_replace('[]','',$content2));
}

function runSection()
{
    add_filter('the_content',   'parseSection');
}

runSection();