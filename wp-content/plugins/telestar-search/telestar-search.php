<?php

/**
 *  Plugin Name: Плагин поиска Телестар
 *  Version: 0.1
 *  Author: EmuSoft Company
 *  Author URI: http://emusoft.pro
 */

/** disable update notification; has to be removed after work */

define('SEARCH_LIMIT', 's_limit');
define('PAGINATION_LIMIT', 'p_limit');

//function remove_core_updates(){
//    global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
//}
//add_filter('pre_site_transient_update_core','remove_core_updates');
//add_filter('pre_site_transient_update_plugins','remove_core_updates');
//add_filter('pre_site_transient_update_themes','remove_core_updates');

/** disable update notification */



add_action('wp_enqueue_scripts', 'callback_for_setting_up_scripts');
function callback_for_setting_up_scripts() {
    wp_register_style( 'front',  plugin_dir_url(__FILE__) . '/css/front.css');
    wp_enqueue_style( 'front' );
    //wp_enqueue_script( 'namespaceformyscript', 'http://locationofscript.com/myscript.js', array( 'jquery' ) );
}

add_action( 'wp_ajax_search', 'search_function'); // wp_ajax_{ЗНАЧЕНИЕ ПАРАМЕТРА ACTION!!}
add_action( 'wp_ajax_nopriv_search', 'search_function');  // wp_ajax_nopriv_{ЗНАЧЕНИЕ ACTION!!}
add_action( 'wp_ajax_searchtip', 'searchtip');
add_action( 'wp_ajax_nopriv_searchtip', 'searchtip');

function getTip($str)
{
    global $wpdb;
    $order = 'region';
    if(preg_match('#^[0-9]+#', $str))
    {
        $order = 'code';
    }
    $query = $wpdb->prepare(  "SELECT DISTINCT region FROM " . $wpdb->prefix."search_plugin" . " WHERE region LIKE  '" . $str  . "%%' OR code LIKE '" . $str  . "%%' ORDER BY " . $order . " ASC" , $str, $str );
//    print_r($query);
    $result['results'] = $wpdb->get_results($query);

    header('Content-Type: text/html; charset=utf-8');
    header('Content-Type: application/json');
    return json_encode($result, JSON_UNESCAPED_UNICODE);
}

function searchtip()
{
    if(isset($_POST['searchstr']))
    {
        $searchStr = trim($_POST['searchstr']);

        //echo $searchStr . " end str";
        if($searchStr != null)
        {
           // echo 'start line_' . $searchStr . '_end line';
            echo getTip($searchStr);

        } else {
            echo 'Некорректный запрос';
        }
    }
    wp_die();
}
function search_function()
{

    global $wpdb;
    $limit =  (get_option('search_plugin_' . SEARCH_LIMIT)) ? get_option('search_plugin_' . SEARCH_LIMIT)  : 20;
    $offset = 0;
    $result = [];

    $order = (isset($_REQUEST['order'])) ? $_REQUEST['order'] : 'region';
    $sort =  (isset($_REQUEST['sort'])) ? $_REQUEST['sort'] : 'ASC';

    $limitPagination = (get_option('search_plugin_' .PAGINATION_LIMIT)) ? get_option('search_plugin_' . PAGINATION_LIMIT)  : 10;

    $str = $_REQUEST['searchstr'];
    $currentPage = $_REQUEST['page'];
    if(preg_match('#^[0-9]+#', $str))
    {
        $order = 'code';
    }
    $offset = ($currentPage != 1 ) ? ($limit * $currentPage) - $limit : 0;
    $getSqlCount = $wpdb->get_results("SELECT COUNT(id) AS count FROM " . $wpdb->prefix . 'search_plugin' . " WHERE region LIKE '" . $str . "%' OR code LIKE '". $str ."%' ");
    $pages = ceil($getSqlCount[0]->count / $limit);

//    if($getSqlCount[0]->count % $limit == 0) $pages--;
    //$result['pages'] = $pages;

    $result['pagination'] = '<div class="paging">';
    $pageNext = 1;
    $pagePrev = 1;
    $pageIndex = 0;

    $blockNum = ceil($currentPage / $limitPagination);
    $startBlock = ($blockNum * $limitPagination) - $limitPagination + 1;
    $startBlock = ($startBlock == 0) ? $startBlock + 1 : $startBlock;
    $lastBlockNum = ceil($pages / $limitPagination);
    $lastBlockNum = ($lastBlockNum == 1) ? $lastBlockNum + 1 : $lastBlockNum;

    /* debug block */
//    $result['pagination'] .=  "pages__" . $getSqlCount[0]->count;
//    $result['pagination'] .=  "  start block__" . $startBlock;
//    $result['pagination'] .=  "  num block__" .  $blockNum;
//    $result['pagination'] .= "   last block num__" . $lastBlockNum;
//    $result['pagination'] .= "<br><br>";

    /* pagination block */

    $sortStr = '&sort=' . $sort;
    $orderStr = '&order=' . $order;
    if($currentPage > $limitPagination)
    {
        $result['pagination'] .= '<a class="paging-nav" href="?page=1&amount=' . $offset . '&q=' . $str . $orderStr . $sortStr . '">1</a>';
    }
    if($blockNum > 1)
    {
        if($blockNum == $lastBlockNum) //если последний блок, распечатаем предыдущий //не распечатывать первый блок
        {
            $prevBlock = $startBlock - $limitPagination;
            if($blockNum - 1 == 1)
            {
                for($i = $prevBlock; $i < $prevBlock + $limitPagination; $i++) {
                    if($i != 1) $result['pagination'] .= '<a class="paging-nav" href="?page=' . $i. '&amount=' . $offset . '&q=' . $str . $orderStr .  $sortStr .'">'.$i.'</a>';
                }
            } else {
                $result['pagination'] .= '<a class="paging-nav" href="?page=' . ($prevBlock - 1) . '&amount=' . $offset . '&q=' . $str . $orderStr .  $sortStr .'">...</a>';
                for($i = $prevBlock; $i < $prevBlock + $limitPagination; $i++) {
                    $result['pagination'] .= '<a class="paging-nav" href="?page=' . $i. '&amount=' . $offset . '&q=' . $str . $orderStr .  $sortStr .'">'.$i.'</a>';
                }
            }
        } else $result['pagination'] .= '<a class="paging-nav" href="?page=' . ($startBlock - 1) . '&amount=' . $offset . '&q=' . $str . $orderStr .  $sortStr .'">...</a>';
    }
    for($i = $startBlock; $i <= $limitPagination * $blockNum; $i++) {
        if($i > $pages)
        {
            break;
        }
        if($currentPage != $i)
        {
            $result['pagination'] .= '<a class="paging-nav" href="?page=' . $i . '&amount=' . $offset . '&q=' . $str . $orderStr .  $sortStr .'">' . $i . '</a>';

        } else {
            $result['pagination'] .= '<span>'. $i .'</span>';
        }

    }

    //последняя страница минус лимит ==> попадает в блок или нет
    //предпоследний блок
    if($blockNum < $lastBlockNum - 1) //все блоки, кроме предпоследнего
    {
        $result['pagination'] .= '<a class="paging-nav" href="?page='. ($startBlock + $limitPagination) .'&amount='.$offset.'&q=' . $str . $orderStr .  $sortStr .'">...</a>';
        $result['pagination'] .= '<a class="paging-nav" href="?page='. $pages .'&amount='.$offset.'&q=' . $str . $orderStr .  $sortStr .'">' . $pages . '</a>';
    } elseif ($blockNum == $lastBlockNum - 1) //предпоследний блок
    {
        $lastBlockElems = $startBlock + $limitPagination;
        for ($i = $lastBlockElems; $i < $limitPagination + $lastBlockElems; $i++) //распечатаем последний блок, начнем со старта последнего блока
        {
            if($i > $pages)
            {
                break;
            }
            $result['pagination'] .= '<a class="paging-nav" href="?page='. $i .'&amount='.$offset.'&q=' . $str . $orderStr .  $sortStr .'">' . $i . '</a>';

        }
    }
    $result['pagination'] .= '</div>';
    $order = sanitize_sql_orderby( $order );
    $sort = sanitize_sql_orderby( $sort );

    $query = $wpdb->prepare(  "SELECT code, country, region, price FROM " . $wpdb->prefix."search_plugin" .
        " WHERE region LIKE '" . $str  . "%%' OR code LIKE '" . $str  . "%%' ORDER BY " . $order . " " . $sort . " LIMIT %d OFFSET %d", $limit, $offset);

//   print_r($query);
    $result['results'] = $wpdb->get_results($query);
    if($result['results'] == null)
    {
        $result['results'] = [["code"=>"Поиск","country"=>"","region"=>" не вернул ","price"=>"результат"]];
//        wp_die();
    }
  //  echo $pages
    header('Content-Type: text/html; charset=utf-8');
    header('Content-Type: application/json');
    echo json_encode($result, JSON_UNESCAPED_UNICODE);

    wp_die();
}

add_action( 'admin_menu', 'register_search_plugin');
add_filter( 'option_page_capability_'.'my_page_slug', 'my_page_capability' );
//error_reporting(0);

function register_search_plugin(){
    global $wpdb;
    //add_options_page ('Тарифы', 'Тарифы', 8, 'telestar_search_set', 'telestar-search_options_page');
    add_menu_page( 'Поиск Телестар. Настройки.', 'Импорт Телестар', 'edit_others_posts', 'telestar_search', 'start_plugin', plugins_url( 'telestar-search/img/icon.png' ), 12 );
    add_submenu_page('telestar_search','Настройки', 'Настройки', 'edit_others_posts', 'search_settings', 'search_settings','', 2);

    $sqlCreateStr =  "CREATE TABLE IF NOT EXISTS `tlst_search_plugin` ( 
                        `id` int(11) NOT NULL AUTO_INCREMENT, `code` varchar(100) DEFAULT NULL, 
                        `region` varchar(245) DEFAULT NULL, `price` double DEFAULT NULL, 
                        `country` varchar(245) DEFAULT NULL, 
                        PRIMARY KEY (`id`), UNIQUE KEY `code_UNIQUE` (`code`)
                        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
    $wpdb->query($sqlCreateStr);
}

//add_action('admin_menu', mainPage2());

function start_plugin()
{

    import_data();
    include "import.phtml";

}

function import_data()
{
    if(isset($_POST['import_search_form_btn'])) {
        set_time_limit(180);
        global $wpdb;
        $uploadOk = 1;
        global $status;

        $status['type'] = 'error';
        $status['text'] = '';
        if (function_exists('current_user_can') && !current_user_can('manage_options')) die (_e('Нет прав'));

        $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/uploads/";
//        echo mb_detect_encoding ($_FILES["search_data"]["name"]);
//        exit;
        //$target_file = $target_dir . iconv("utf-8", "windows-1251", basename($_FILES["search_data"]["name"]));
        $target_file = $target_dir . basename($_FILES["search_data"]["name"]);
        if (!is_dir($target_dir) && !is_writable($target_dir)) {
            $status['text'] = 'Невозможно  загрузить файл в каталог ' . $target_dir;
            $uploadOk = 0;
        }

        $mimeType = trim($_FILES['search_data']['type']);
//        if(preg_match('#\.csv$#', $_FILES["search_data"]["name"]))
//        {
//            echo  "YES";
//        }
//        print_r(preg_match('#\.csv$#', $_FILES["search_data"]["name"]));
        if($mimeType != 'application/vnd.ms-excel' && !preg_match('#\.csv$#', $_FILES["search_data"]["name"]))
        {
            $status['text'] =  'Проверьте расширение импорта!';
            $uploadOk = 0;
        }
        if ($_FILES["search_data"]["size"] > 2200000)
        {
            $status['text'] = "Извините, файл слишком большой";
            $uploadOk = 0;
        }

        if($uploadOk != 0) {
            if (move_uploaded_file($_FILES["search_data"]["tmp_name"], $target_file)) {
                $status['type'] = 'success';
                $status['text'] = "Файл " . basename($_FILES["search_data"]["name"]) . " был загружен успешно";
            } else {
                $status['text'] = "Файл не был перемещен  в " . $target_file;
            }
        } else {
            return $status;
        }

        $wpdb->query('TRUNCATE TABLE ' . $wpdb->prefix . 'search_plugin');
        foreach (file($target_file) as $value) {
            if($value != "\r\n")
            {
                if(!mb_detect_encoding($value, 'UTF-8', true)) {
                    $value = mb_convert_encoding($value, "UTF-8", "Windows-1251");
                }

                $record = str_getcsv($value, ";", '"');
                $tmpArr['code'] = $record[0];
                $tmpArr['region'] = $record[1];
                $tmpArr['price'] = str_replace(',', '.',  $record[2]);

                $wpdb->replace($wpdb->prefix.'search_plugin', $tmpArr);
            }
        }

//        if (function_exists('check_admin_referer')) {
//            //check_admin_referer('setup_tariff_form');
//
//        }
    }
}

/* settings page */
add_action( 'wp_ajax_searchsettings', 'search_settings_save');
add_action( 'wp_ajax_nopriv_searchsettings', 'search_settings_save');
function search_settings()
{
    //search_settings_save();
    include "settings.phtml";

   // echo "<div>Страница настроек поиска</div>";
}

function search_settings_save()
{
    if(isset($_POST[SEARCH_LIMIT]))
    {
        $limit = trim($_POST[SEARCH_LIMIT]);
        $pLimit = trim($_POST[PAGINATION_LIMIT]);

        if($limit != null) {
            update_option('search_plugin_' . SEARCH_LIMIT , $limit);
            update_option('search_plugin_' . PAGINATION_LIMIT , $pLimit);
        } else {
            echo 'Некорректный запрос';
        }
    }
    wp_die();
}